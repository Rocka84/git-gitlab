## Intro

Simple git extension to interact with GitLab from command line.

API: https://gitlab.com/gitlab-org/gitlab-ce/tree/master/doc/api

### Installation

~~~
sudo apt-get install python-pip
sudo pip install git-gitlab
~~~

## Configuration

~~~
global:
git config --global gitlab.url    "http://your-server-url"
git config --global gitlab.token  "your-token"

local in each repository:
git config gitlab.url   "http://your-server-url"
git config gitlab.token "your-token"
~~~

## Usage

The first configured remote will be used to determine gitlab repository by
remote url.

### All commands

~~~
git lab -h
~~~

For each command get help by git gitlab command -h
and further infos what needs to be done is provided.

For example creating merge request.
If you are on the repository cloned and tracked on any gitlab server
referenced as remote.

~~~
git lab mr
~~~

All parameters are optional source, target branch and title 
if not provided will be derived from repository and current 
branch itself.

## Executing tests

Install requirements:

`pip install mock unittest2`

Execute tests from within `gitgitlab`:

`python -m unittest2 test/gitgitlab_test.py`
